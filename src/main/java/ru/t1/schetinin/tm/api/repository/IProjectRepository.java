package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

}